/*
  IoT Server - Light switch client
  Copyright (C) 2016  Alex Rhodes
  https://www.alexscottrhodes.com
  
  Information about this project including set-up and configuration information can be found here:
  https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/overview
  
  This script, to be run on a web-connected arduino microcontroller, is an example of a simple HTTP client of the web service
    
  IoT Server - Lightswitch client
  Copyright (C) 2016  Alex Rhodes
  https://www.alexscottrhodes.com
  
  Information about this project including set-up and configuration information can be found here:
  https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/
  
  This page is an example of a simple http client for the IoT web service.
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include <SPI.h>
#include <Ethernet.h>
//Set a unique name to assign to your device
String deviceName = "arduino-lightswitch";

//Set host and port where your web service is running
char registration_address[] = "192.168.0.8";
int servicePort = 8080;

//Set the mac address and desired IP address of the service
byte mac[] = {0xAA, 0xAA, 0xBB, 0xBB, 0xCC, 0xCC};
IPAddress ip(192, 168, 0, 35);
EthernetServer server(80);
EthernetClient client;
int lightPin = 8;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  delay(500);
  Serial.println("Server starting..");
  Ethernet.begin(mac, ip);
  String ip = ipToString(Ethernet.localIP());
  //Register with the web service
  registerDevice(ip,deviceName);
  server.begin();
  Serial.print("Server started at ");
  Serial.println(Ethernet.localIP());
  pinMode(lightPin,OUTPUT);

}

void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
     String input = "";
      if (client.available()) {
        input = client.readStringUntil('\r');
        Serial.println(input);
        delay(10);
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("Arduino server");
          client.println("</html>");
        }
    delay(1);
    client.stop();
    Serial.println("client disconnected");
    if(input.indexOf("light=off")!= -1){
        Serial.println("Turning off");
        digitalWrite(lightPin,LOW);
      }
    if(input.indexOf("light=on") != -1){
        Serial.println("Turning on");
        digitalWrite(lightPin,HIGH);
      }
  }
}

//Register the device's unique name and IP address via the web service REST API
void registerDevice(String ip, String deviceName){
  Serial.println("Registering " + deviceName + " with service at " + ip);
  if(client.connect(registration_address,servicePort)){
    Serial.println("connected..");
    client.println("GET /IoT-Server/rest/insecure/register?name="+deviceName+"&ip="+ip+" HTTP/1.1");
    client.println("Host: " + String(registration_address));
    client.println("Connection: close");
    client.println();
    Serial.println("registered");
    }
}
//Utility to convert IP address to a string
String ipToString(const IPAddress& ipAddress)
{
  return String(ipAddress[0]) + String(".") +\
  String(ipAddress[1]) + String(".") +\
  String(ipAddress[2]) + String(".") +\
  String(ipAddress[3])  ; 
}


