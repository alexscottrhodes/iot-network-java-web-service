# IoT Network Java Web Service - 1.0 #
	IoT Server - Lightswitch client
	Copyright (C) 2016  Alex Rhodes
	https://www.alexrhodes.io

	Information about this project including set-up and configuration information can be found here:
	https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>. 
## Introduction ##
This project is designed to serve a skeletal platform for a REST Internet-of-Things web service. The goals of the application are as follows:

+ Allow any device capable of HTTP connections to send and receive commands from any other device on the network. This inlcudes micro-controllers, mobile applications, desktop computers or any other device capable of sending GET or POST requests.

+ Provides IP Address independent device lookup, allowing devices to register a unique name and IP address pair with the service when connecting or reconnecting so that other devices may communicate with them via their name regardless of their impermanent IP address.

+ Provide database integration within the IoT environment to allow things like device lookup, command storage, configuration settings etc.

This service was designed to be a foundation for an IoT server project. It provides opportunities for implementing many features to improve simplistic device-to-device implementations. For example:
 - integrating command storage and logging, i.e commands sent to devices that are offline will be stored and sent when the device re-connects
 - exposure to the web via REST API interfaces allowing communication with the device from outside the local network
 - Centralized configuration settings and the option of creating a central control interface. For example, a web GUI to allow the configuration and scheduling of an automatic light controller, and centralized storage of these settings available for look up by all of the controllers on the network.  

## Assumptions and Expectations ##
This information is provided for individuals with experience in programming, some knowledge of HTTP communication, RESTful web services, and various programming and development technologies and environments. If you are unfamiliar with any of the concepts mentioned below, there are numerous resources available online to learn. I am willing to assist with minor technical issues or questions about the service but I will refer you to online resources for basic questions about getting started with the technologies mentioned below, or programming in general. I provided links that I hope will be helpful in the sections below. 
**Feel free to contact me at alexrhodes@live.com**

## Configuration ##
This section will provide information on configuring and running the project. These instructions are not exhaustive, but provide links to 3rd party, detailed configuration tutorials.

## Requirements ##
The web service provided is written in Java. The project is integrated with the Maven build management system. Dependencies are managed by Maven. The service is designed to run on a Java application server. This project was specifically developed and tested on Apache Tomcat. The service requires a connection to a MySQL or other SQL server. The example client provided is designed to operate on an Arduino micro-controller, and the example control client runs in most web browsers. These example clients can be easily adapted or replaced to suit your needs. 

## Set Up Process ##
Within each of the files mentioned in this section, there are comments with information about usage and configuration. Please view these files for more information than what is provided below.
The project should be imported into a Maven compatible Java development environment, such as Eclipse. The database persistence connection information must be updated in the persistence.xml file located in *~project root~/ src / main / resources / META-INF / persistence.xml*
The file will need the URL path to your MySQL connection, as well as the authentication information. The project should be deployed to an application server. There may be additional configuration per your particular environment. 
More information (I do not own these links and they may not be current, please let me know if you find a dead link):

 + Maven build managment -  http://www.tutorialspoint.com/maven/
 
 + Java Persistence - http://www.tutorialspoint.com/jpa/
 
 + Java web service deployment (tomcat) - https://examples.javacodegeeks.com/enterprise-java/jws/jax-ws-web-services-on-tomcat/
 
 + MySQL server set up and config (Apache XAMPP) - https://blog.udemy.com/xampp-tutorial/ 

The example client provided under *~project root~/Client Example/Arduino_Lightswitch* is a simple script designed for the Arduino micro-controller that has either a WiFI or Ethernet connection "shield" allowing it to operate as a web client and server. The script will need to be configured with your desired name for the client, the IP address to assign to it, a MAC address to assign, and the Ip Address and port where the web service is operational. This client is simply provided as example use case for this service. 
More information (I do not own these links and they may not be current, please let me know if you find a dead link):

+ The Arduino micro-controller - https://www.arduino.cc/en/Guide/Introduction

+ Ethernet shield -  https://www.arduino.cc/en/Main/ArduinoEthernetShield

The example control client under *~project root~/Client Example/Light Switch.html* will run in any browser.It will require that the URLs within be updated with the url of your web service and desired commands. 

## Usage ##
The service operates four REST API functions at four URLs:
### Device Registration###
 **~Context Root~/rest/insecure/register** 

This URL allows a device to register it's name and IP address with the web service when it connects. The name and IP address are passed in query parameters of the form:

*name=<device name>&ip=<device ip>*

An example of this URL would be:

http://192.168.0.8:8080/IoT-Server/rest/insecure/register?name=arduino-lightswitch&ip=192.168.0.13

This API call's purpose is to allow a newly connected device to register it's IP address whenever it connects to the network. The purpose of this registration is to allow other devices to target commands to this device without needing to know the device's actual IP address (Because it may change). The only parameter the sending device needs to know is the device's permanent name. In my example client, the device's name is "arduino-lightswitch" and all commands addressed to this device are pointed to it's name. The IP address and actual connection process is handled by the webservice. 

### Command Send###
 **~Context Root~/rest/insecure/send** 

This URL allows a device to send a named command to a device by specifying the target device's name, the command name, and the value of the command. These values are passed as parameters in the form:

*device=<target device>&command=<command name>&value=<command value>*

An example of this URL would be:

http://192.168.0.8:8080/IoT-Server/rest/insecure/send?device=arduino-lightswitch&command=light&value=off

This API call's purpose is to allow a device to send a command to another device. The command name and value is sent to the web service, along with the name of the target device. The service looks up the device's ip address information and handles the connection. The service relays the device's response back to the caller. The commands are sent to the receiving device via GET parameter in the url in the form:

*name=value*

An example URL a device will be called at by the service is: 

https://192.168.0.35?light=on

The device is then responsible for parsing and reacting to these GET parameters.

### Send Email###

**~Context Root~/rest/insecure/sendEmail?**

**~Context Root~/rest/insecure/sendEmailTo?** 

This URL allows a device to send notification emails. This call relies on using a Gmail account with external SMTP access enabled. The Gmail email address and passowrd must be hard coded
into the EmailUtility.java file, or provided to the email utility by some other means. 

One end point is designed to use a pre-configured email address that is either hard-coded or retrieved from some resource, for example, a configuration database. This URL is of the form:

*/sendEmail?device=<sending device name>&message=<message>*

The other end point can have the destination email address passed to it by the device, it is of the form:

*/sendEmail?to=<destination email address>&device=<sending device name>&message=<message>*

