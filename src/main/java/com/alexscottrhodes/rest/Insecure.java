/*
	IoT Server
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	Information about this project including set-up and configuration information can be found here:
	https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/overview
		
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package com.alexscottrhodes.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import com.alexscottrhodes.model.Command;
import com.alexscottrhodes.model.Device;
import com.alexscottrhodes.model.TransactionResult;
import com.alexscottrhodes.service.CommandFunctions;
import com.alexscottrhodes.service.DeviceFunctions;
import com.google.gson.Gson;

import com.alexscottrhodes.model.Email;
import com.alexscottrhodes.model.NotificationEmail;

/**
 * The main endpoint for devices connecting to the IoT network for devices using GET parameters to pass data. The REST calls here do not require any type of authentication.
 * @author Alex Rhodes
 *
 */


@Path("/insecure")
public class Insecure {
	private Gson gson = new Gson();
	/**
	 * A utility URL that returns a 200 response when the webservice is up and running.
	 * @return a Response of 200 indicating the web service is available
	 */
	@GET
	@Path("/")
	@Produces("text/html")
	public Response insecure(){
		return Response.ok().build();
	}
	
	/**
	 * Method for devices to register their IP address with the service when first establishing a connection to the network.
	 * @param name a String representing the permanent name of the device that other devices on the network use when targeting it
	 * @param ipAddress a String of the device's IP address on the IoT network
	 * @return an HTTP Response indicating success or failure of the decvice's registration.
	 */
	@GET
	@Path("/register")
	public Response registerDevice( @QueryParam("name") String name, @QueryParam("ip") String ipAddress){
		try{
		if(DeviceFunctions.registerDevice(name,ipAddress)){
			return Response.ok().build();
		}else{
			return Response.status(500).entity("There was a problem registering the device").build();
		}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).entity("There was a problem registering the device").build();
		}
	}
	
	/**
	 * Method for sending a command to a device. The target device, command name and value are passed. The commands are stored in the database. Commands are then sent to the target device.
	 * If the device can not be reached, the commands are stored. If they device provides a response, this response and the response code is passed to the sending device. 
	 * @param deviceName the Device identifier for the target device
	 * @param commandName the Command identifier for the command being sent
	 * @param commandValue the Command value to send
	 * @return a Response with an HttpTransaction object in JSON format providing successful and failed transaction information and device responses to the command.
	 */
	@GET
	@Path("/send")
	public Response sendCommand(@QueryParam("device") String deviceName, @QueryParam("command") String commandName, @QueryParam("value") String commandValue){
		try{
			Device d = DeviceFunctions.getDeviceByName(deviceName);
			if(d == null){
				return Response.status(400).entity("Device not found.").build();
			}
			Command command = new Command();
			command.setCommandName(commandName);
			command.setCommandValue(commandValue);
			command.setTargetDevice(d);
			command = CommandFunctions.storeCommand(command);
			TransactionResult tr = CommandFunctions.sendCommands(command,d);
			String trJson = gson.toJson(tr);
			if(tr.isSuccessful()){
				return Response.status(200).entity(trJson).build();
			}else{
				return Response.status(400).entity(trJson).build();
			}
		
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).entity("There was a problem sending the commands").build();
		}
	}

	/**
	 * Method providing email functionality to IoT Devices
	 * @param deviceName a String representing the name of the device sending the message
	 * @param message a String of the message to be sent
	 * @return a Response indicating success or failure of the sent message.
	 */
	@GET
	@Path("/sendMail")
	public Response sendMail(@QueryParam("device") String deviceName, @QueryParam("message") String message){
		try{
			Email email = new NotificationEmail("<to-email-from-some-resource>","Message from " + deviceName,message);
			email.send();
			return Response.ok().build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).entity("There was a problem sending the notification.").build();
		}
	}
	
	/*
	 * Method providing email functionality to IoT Devices
	 * @param toEmail a String of the recipient email address
	 * @param deviceName a String representing the name of the device sending the message
	 * @param message a String of the message to be sent
	 * @return a Response indicating success or failure of the sent message.
	 */
	@GET
	@Path("/sendMailTo")
	public Response sendMailWithAddress(@QueryParam("to") String toEmail, @QueryParam("device") String deviceName, @QueryParam("message") String message){
		try{
			Email email = new NotificationEmail(toEmail,"Message from " + deviceName,message);
			email.send();
			return Response.ok().build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).entity("There was a problem sending the notification.").build();
		}
	}
	
	
	
	
}
