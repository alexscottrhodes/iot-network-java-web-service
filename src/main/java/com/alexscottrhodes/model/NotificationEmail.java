package com.alexscottrhodes.model;

import com.alexscottrhodes.utility.EmailUtility;

public class NotificationEmail extends Email{

	public NotificationEmail(String toAddress, String subject, String body) {
		super(toAddress, null, subject, body);
	}
	
	@Override
	public boolean send(){
		return EmailUtility.sendEmail(this);
	}

}
