/*
	IoT Server
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	Information about this project including set-up and configuration information can be found here:
	https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/overview
		
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package com.alexscottrhodes.model;

import java.util.ArrayList;
/**
 * An object that stores HttpResult objects to be sent back to the sender when multiple HTTP transactions occur with receivers.
 * @author Alex Rhodes
 *
 */

public class TransactionResult {
	boolean successful;
	ArrayList<HttpResult> success = new ArrayList<HttpResult>();
	ArrayList<HttpResult> fail = new ArrayList<HttpResult>();
	
	public ArrayList<HttpResult> getSuccess() {
		return success;
	}
	public void setSuccess(ArrayList<HttpResult> success) {
		this.success = success;
	}
	public ArrayList<HttpResult> getFail() {
		return fail;
	}
	public void setFail(ArrayList<HttpResult> fail) {
		this.fail = fail;
	}
	public boolean isSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	
}
