/*
	IoT Server
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	Information about this project including set-up and configuration information can be found here:
	https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/overview
		
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package com.alexscottrhodes.model;

/**
 * An object to hold information about a given HTTP request interaction with a device, including a boolean for the success
 * of the transaction, the HTTP response code, the HTTP message, and if applicable, the device or service's response.
 * @author Alex Rhodes
 *
 */
public class HttpResult {
	
	private boolean successful;
	private int responseCode;
	private String response;
	private String httpMessage;
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public boolean getSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	public String getHttpMessage() {
		return httpMessage;
	}
	public void setHttpMessage(String httpMessage) {
		this.httpMessage = httpMessage;
	}
}
