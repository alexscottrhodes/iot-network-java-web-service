/*
	IoT Server
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	Information about this project including set-up and configuration information can be found here:
	https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/overview
		
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

 */
package com.alexscottrhodes.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.alexscottrhodes.model.Device;

/**
 * Contains functions relating to IoT devices on the network
 * 
 * @author Alex Rhodes
 *
 */

public class DeviceFunctions {
	private static EntityManagerFactory emFactory;
	private static EntityManager em;

	
	/**
	 * Register a device with the network. If the device already exists under the unique name, update its IP address information is updated.
	 * If the device does not exist, a new one is created.
	 * @param a String of the name a Unique identifier for the device
	 * @param a String of the ipAddress the IP address for the device
	 * @return a Boolean indicating a successful update
	 */
	public static boolean registerDevice(String name, String ipAddress){
		emFactory = Persistence.createEntityManagerFactory("iot_persistence");
		em = emFactory.createEntityManager();
		em.getTransaction().begin();
		Device d = new Device();
		boolean exists = false;
		try{
			Query q  = em.createNamedQuery("getDeviceByName").setParameter("name", name);
			d = (Device) q.getSingleResult();
			exists = true;
		}catch(Exception e){ //Exception if no result
			exists = false;
		}
		if(exists){
			d.setIpAddress(ipAddress);
		}else{
			d.setName(name);
			d.setIpAddress(ipAddress);
		}
		em.persist(d);
		em.flush();
		em.getTransaction().commit();
		em.close();
		emFactory.close();
		return true;
	}
	
	/**
	 * Get a device by it's unique name
	 * @param name a String of the device's unique name
	 * @return a Device object with the given name
	 */
	public static Device getDeviceByName(String name){
		emFactory = Persistence.createEntityManagerFactory("iot_persistence");
		em = emFactory.createEntityManager();
		em.getTransaction().begin();
		Device d = new Device();
		boolean exists = false;
		try{
			Query q  = em.createNamedQuery("getDeviceByName").setParameter("name", name);
			d = (Device) q.getSingleResult();
			exists = true;
		}catch(Exception e){ //Exception if no result
			exists = false;
		}
		em.persist(d);
		em.flush();
		em.getTransaction().commit();
		em.close();
		emFactory.close();
		if(exists){
			return d;
		}else{
			return null;
		}
	}
	
	
	
	
}
