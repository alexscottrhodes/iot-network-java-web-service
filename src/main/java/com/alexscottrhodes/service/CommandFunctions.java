/*
	IoT Server
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	Information about this project including set-up and configuration information can be found here:
	https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/overview
		
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package com.alexscottrhodes.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.alexscottrhodes.model.Command;
import com.alexscottrhodes.model.Device;
import com.alexscottrhodes.model.HttpResult;
import com.alexscottrhodes.model.TransactionResult;
import com.alexscottrhodes.utility.HttpFunctions;

/**
 * Contains functions relating to command objects
 * 
 * @author Alex Rhodes
 *
 */

public class CommandFunctions {
	private static EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("iot_persistence");
	private static EntityManager em;
	
	
	/**
	 * Store a new command in the database
	 * @param c a Command to be stored
	 * @return a Command that is merged with the persistence context
	 */
	public static Command storeCommand(Command c){
		em = emFactory.createEntityManager();
		em.getTransaction().begin();
		c = em.merge(c);
		em.getTransaction().commit();
		return c;
	}
	
	/**
	 * Send a command to the target device. If the command is sent successfully, it is purged from the device's commands, 
	 * otherwise it is stored until it can be successfully sent.
	 * @param c the Command to send
	 * @param d a Device to send the command to.
	 * @return a TransactionResult object containing a list of failed and successful HttpResult objects
	 */
	public static TransactionResult sendCommands(Command c, Device d){
		TransactionResult tr = new TransactionResult();
		em = emFactory.createEntityManager();
		em.getTransaction().begin();
		HttpResult hr = HttpFunctions.sendGet(d, c);
		if(hr == null){
			return null;
		}
		if(hr.getSuccessful()){
			tr.getSuccess().add(hr);
			c = em.find(Command.class, c.getId());
			em.remove(c);			
		}else{
			tr.getFail().add(hr);
		}
		em.getTransaction().commit();
		return tr;
	}
	
}
