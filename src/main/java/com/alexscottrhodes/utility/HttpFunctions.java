/*
	IoT Server
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	Information about this project including set-up and configuration information can be found here:
	https://bitbucket.org/alexscottrhodes/iot-network-java-web-service/overview
		
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package com.alexscottrhodes.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.alexscottrhodes.model.Command;
import com.alexscottrhodes.model.Device;
import com.alexscottrhodes.model.HttpResult;

/**
 * Contains methods for HTTP communication functions
 * @author Alex Rhodes
 *
 */
public class HttpFunctions {

	/**
	 * Sends a GET command to the device specified with the command parameters specified. 
	 * @param d a Device to send the command to
	 * @param c a Command to send
	 * @return an HttpResult object with the connection status and device response or error response.
	 */
	public static HttpResult sendGet(Device d, Command c){
		HttpResult result = new HttpResult();
		String query = "?";
		query += c.getCommandName() + "=" + c.getCommandValue();
		try{
			String urlString = d.getIpAddress() + query;
			urlString = "http://" + urlString;
			URL url = new URL(urlString);
	    	HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setConnectTimeout(5000);
			int responseCode = 500;
			try{
				con.connect();
				responseCode = con.getResponseCode();
			}catch(java.net.ConnectException e){
				result.setResponseCode(408);
				result.setHttpMessage("The device could not be reached.");
				result.setSuccessful(false);
				return result;
			}
			if(responseCode != 200){
				result.setResponseCode(responseCode);
				result.setHttpMessage(con.getResponseMessage());
				result.setSuccessful(false);
				return result;
			}
			BufferedReader inputResponse = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = inputResponse.readLine()) != null) {
				response.append(inputLine);
			}
			inputResponse.close();
			result.setResponse(response.toString());
			result.setResponseCode(200);
			result.setSuccessful(true);
			return result;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}		
	}
	
	
	
}
