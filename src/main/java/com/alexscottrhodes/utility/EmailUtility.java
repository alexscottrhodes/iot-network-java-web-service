package com.alexscottrhodes.utility;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import com.alexscottrhodes.model.Email; 

public class EmailUtility {
	private static final String gmailAddress = "<your gmail address>";
	private static final String gmailPassword = "<your gmail password>";
	
	public static boolean sendEmail(Email email){
		
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(gmailAddress,gmailPassword);
				}
			});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(gmailAddress));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(email.getToAddress()));
			message.setSubject(email.getSubject());
			message.setText(email.getBody());
			Transport.send(message);

			return true;

		} catch (MessagingException e) {
			return false;
		}
	}
	
}
